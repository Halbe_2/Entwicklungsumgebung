import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;
/**
 * Dies ist ein Dialogprogramm zum starten der Entwicklungsumgebung
 */
public class Program {

    private int funktion = -1;
    private static String currentOP;
    private static final int CODE_GEN = 1;
    private static final int BUILD_MAVEN = 2;
    private static final int BUILD_ANT = 3;
    private static final int START_PROGRAM = 4;
    private static final int START_TEST = 5;
    private static final int BUILD_JAR = 6;
    private static final int CLEAN = 7;

    private static final int CHECKOUT = 8;
    private static final int GET_HISTORY = 9;

    private static final int START_IDE = 10;
    private static final int GENERATE_UML = 11;

    private static final int END = 0;
    private FileController controller;
    /**
     * Mit dieser Methode führen wir das Programm aus
     */
    public void start(String path){
        controller = new FileController(path);
        while(funktion!=END){
            try{
                funktion = menue();
                ausfuehrung(funktion);
            }
            catch(Exception e){
                System.out.println(e);
            }
        }
    }

    /**
     * Einstiegspunkt im Programm
     * @param args übergebene Parameter
     * @throws Exception
     */
    public static void main(String[] args) throws Exception{
        if(args.length == 1){
            currentOP = System.getProperties().getProperty("os.name");
            String fs = System.getProperty("file.separator");
            String path = new BufferedReader(new InputStreamReader(new ProcessBuilder("pwd").start().getInputStream())).readLine();
            if(currentOP.contains("Windows")){
                Runtime.getRuntime().exec("Set_Win.cmd");
            }else if(currentOP.contains("Linux")){
                new ProcessBuilder("./Scripts/Set_Linux.sh").start();
            }
            new Program().start(path+fs+args[0]);
        }
        System.err.println("Kein Projekt angegeben");
    }

    public int menue(){
        System.out.println(CODE_GEN+". Dokumentation erstellen");
        System.out.println(BUILD_MAVEN+". Maven build erstellen");
        System.out.println(BUILD_ANT+". Ant build erstellen");
        System.out.println(START_PROGRAM+". Programm straten");
        System.out.println(START_TEST+". Unit-Tests starten");
        System.out.println(BUILD_JAR+". JAR-Datei generieren");
        System.out.println(CLEAN+". Clean Projekt");
        System.out.println(END+". Schließen");
        return readInt();
    }

    public int readInt(){
        return new Scanner(System.in).nextInt();
    }

    public void ausfuehrung(int eingabe)throws Exception{
        switch (eingabe){
            case CODE_GEN:
                if(currentOP.contains("Linux")){
                    new ProcessBuilder("./Scripts/Code_Gen.sh").start();
                } else if (currentOP.contains("Windows")){
                    new ProcessBuilder("\\Scripts\\Code_Gen.cmd").start();
                }
                break;
            case BUILD_MAVEN:
                if(currentOP.contains("Linux")){
                    new ProcessBuilder("./usr/bin/mvn","").start();
                } else if (currentOP.contains("Windows")){
                    new ProcessBuilder("\\Scripts\\Code_Gen.cmd").start();
                }
                break;
        }
    }
}
