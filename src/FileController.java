
import java.io.*;

public class FileController {

    FileWriter writer;
    File file;
    private String path;

    private static final String CODE_GEN_PATH_LINUX = "Scripts/Code_Gen.sh";
    private static final String CODE_GEN_PATH_WIN = "Scripts\\Code_Gen.cmd";

    private static final String[] CODE_PATHS = {CODE_GEN_PATH_LINUX, CODE_GEN_PATH_WIN };

    public FileController(String path){
        setProjektPath(path);
        for(String string : CODE_PATHS){
            file = new File(string);
            checkFile(file);
            writeToFile(file);
        }
    }


    private boolean checkFile(File file){
        if (file != null) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.err.println("Error creating " + file.toString());
            }
            if (file.isFile() && file.canWrite() && file.canRead())
                return true;
        }
        return false;
    }

    private void writeToFile(File file){
        try{
            if(file.getName().contains("Code_Gen.sh")){
                writer = new FileWriter(file, false);
                writer.write("#!/usr/bin/env bash\n");
                writer.write("$JAVA_HOME/bin/javadoc " +
                        path+"/*.java "+
                        path+"/generated/*.java " +
                        "-d " + path + "/javadoc --add-modules ALL-SYSTEM");
                writer.flush();
                System.out.println(file.getAbsolutePath());
                writer.close();
            }
            if(file.getName().contains("Code_Gen.cmd")) {
                writer = new FileWriter(file, false);
                writer.write("@echo off\n");
                writer.write("%JAVA_HOME%\\bin\\javadoc " +
                        path + "\\*.java " +
                        path + "\\generated\\*.java " +
                        "-d " + path + "\\1-JAXB\\javadoc --add-modules ALL-SYSTEM");
                writer.flush();
                System.out.println(file.getAbsolutePath());
                writer.close();
            }

        }catch (IOException e){
            System.err.print("Error "+e);
        }
    }

    public void setProjektPath(String path){
        this.path = path;
    }

    public String setProjektPath(){
        return path;
    }

}
